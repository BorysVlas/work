<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Boris',
            'login' => 'admin',
            'email' => str_random(10).'@gmail.com',
            'role' => 'admin',
            'password' => Hash::make('admin'),
        ]);
    }
}
