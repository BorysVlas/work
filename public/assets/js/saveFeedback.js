
(function ($) {
	$(document).ready(function () {
		$('#feedback').on('submit', function (e) {
			e.preventDefault();
			
			$.ajax({
				type: 'POST',
				url: '/feedback',
				data: $('#feedback').serialize(),
				success: function (data) {
					console.log('true');
					$('#sendmessage').modal("show");
				},
				error: function () {
					console.log('false');
					$('#senderror').modal("show");
				}
			});
		});
	});
})(jQuery);