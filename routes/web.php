<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'User\UserViewController@showUserView');
Route::get('index', 'User\UserViewController@showUserView');
Route::get('about', 'User\UserViewController@showUserView');
Route::get('blog-grid', 'User\UserViewController@showUserView');
Route::get('blog-sidebar', 'User\UserViewController@showUserView');
Route::get('blog-single', 'User\UserViewController@showUserView');
Route::get('case-single', 'User\UserViewController@showUserView');
Route::get('case', 'User\UserViewController@showUserView');
Route::get('contact', 'User\UserViewController@showUserView');
Route::get('gallery', 'User\UserViewController@showUserView');
Route::get('service-single', 'User\UserViewController@showUserView');
Route::get('service', 'User\UserViewController@showUserView');
Route::get('team', 'User\UserViewController@showUserView');


Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'admin'], function(){
    Route::get('admin', 'Admin\AdminControllerView@index')->name('adminIndex');
    Route::get('blank', 'Admin\AdminControllerView@blank')->name('adminBlank');
    Route::get('buttons', 'Admin\AdminControllerView@buttons')->name('adminButtons');
    Route::get('charts', 'Admin\AdminControllerView@charts')->name('adminCharts');
    Route::get('forms', 'Admin\AdminControllerView@forms')->name('adminForms');
    Route::get('grid', 'Admin\AdminControllerView@grid')->name('adminGrid');
    Route::get('icon', 'Admin\AdminControllerView@icon')->name('adminIcon');
    Route::get('notifications', 'Admin\AdminControllerView@notifications')->name('adminNotifications');
    Route::get('panels', 'Admin\AdminControllerView@panels')->name('adminPanels');
    Route::get('tables', 'Admin\AdminControllerView@tables')->name('adminTables');
    Route::get('typography', 'Admin\AdminControllerView@typography')->name('adminTypography');
    Route::get('email', 'Admin\AdminControllerView@email')->name('adminEmail');
});

Route::post('/feedback', 'FeedbackController@save');
