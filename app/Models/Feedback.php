<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Feedback extends Model
{
    public static function saveEmail($email){
        DB::table('feedback')->insert(
            ['email' => $email]
        );
    }
}
