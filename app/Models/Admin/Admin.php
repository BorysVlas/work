<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Admin extends Model
{
    public static function getEmail(){
        return DB::table('feedback')->pluck('email');
    }
}
