<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Admin;

class AdminControllerView extends Controller
{
    public function index(){
        return view('admin.index');
    }
    public function blank(){
        return view('admin.blank');
    }
    public function buttons(){
        return view('admin.buttons');
    }
    public function charts(){
        return view('admin.charts');
    }
    public function forms(){
        return view('admin.forms');
    }
    public function grid(){
        return view('admin.grid');
    }
    public function icon(){
        return view('admin.icon');
    }
    public function notifications(){
        return view('admin.notifications');
    }
    public function panels(){
        return view('admin.panels');
    }
    public function tables(){
        return view('admin.tables');
    }
    public function typography(){
        return view('admin.typography');
    }
    public function email(){
        $email = Admin::getEmail();
        return view('admin.email', ['emails' => $email]);
    }
    public function showAdminView($data = null, Request $request){
        $path = $request->path();
        return view("Admin.$path", ['data' => $data]);
    }
}
