<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;

class UserViewController extends Controller
{
    public function showUserView(Request $request){
        $path = $request->path();
        if ($path == "/")
            $path = 'index';
        return view("User.$path");
    }
}
