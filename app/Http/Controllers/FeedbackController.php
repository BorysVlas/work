<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feedback;

class FeedbackController extends Controller
{
    public function save(Request $request){
        $email = $request->input('EMAIL');
        Feedback::saveEmail($email);
        return response()->json('sucсess', 200);
    }
}
