<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.indexStyle')

</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

     <!-- ***************************
        Header Area Start
     *************************** -->

     @include ('layouts.user.indexHeader')

     <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Header Slider Area Start
     *************************** -->
   <section class="rev_slider_area" id="rev_slider_1">
        <div class="follow_box">
            <a href="#">Twitter</a>
            <a href="#">Facebook</a>
            <a href="#">Linkedin</a>
        </div><!--end .follow_box-->
        <article class="content">

            <div id="rev_slider_wrapper_main_div" class="rev_sliders_scp rev_slider_wrapper rev_slider_banner fullwidthbanner-container" data-alias="classic4export" data-source="gallery">
                <div id="rev_slider_1078_1" class="rev_slider rev_slider_display fullwidthabanner" data-version="5.4.1">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-10" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                            <!-- MAIN IMAGE -->
                            <img src="assets/images/sliders/slider-1.jpg"  alt="" title="slider-1"  width="1894" height="1043" data-bgposition="center center" data-kenburns="on" data-duration="8000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="9" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption  yellow_tx tp-resizeme"
                                 id="slide-10-layer-10"
                                 data-x="['left','left','left','left']" data-hoffset="['15','15','20','10']"
                                 data-y="['top','top','top','top']" data-voffset="['371','341','300','260']"
                                 data-fontsize="['26','26','20','22']"
                                 data-lineheight="['24','24','22','22']"
                                 data-color="#ffed50"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[2,2,2,2]"
                                 data-paddingleft="[0,0,0,0]">
                                 We Make Difference
                             </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption big_tx  tp-resizeme"
                                 id="slide-10-layer-3"
                                 data-x="['left','left','left','left']" data-hoffset="['15','15','20','10']"
                                 data-y="['top','top','middle','middle']" data-voffset="['410','386','10','-5']"
                                 data-fontsize="['70','70','42','38']"
                                 data-lineheight="['80','80','52','40']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-color="#ffffff"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"y:-50px;opacity:0;","ease":"nothing"}]'
                                 data-textAlign="['left','left','left','left']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                 Leading in finance<br>
                                and consulting<br>
                                business
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption paragraph_tx  tp-resizeme"
                                 id="slide-10-layer-21"
                                 data-x="['left','left','left','left']" data-hoffset="['15','15','20','10']"
                                 data-y="['top','top','middle','middle']" data-voffset="['676','654','125','114']"
                                 data-fontsize="['20','20','16','24']"
                                 data-lineheight="['32','32','30','32']"
                                 data-color="['rgba(255,255,255,0.7)','rgba(255,255,255,0.7)','rgba(255,255,255,0.7)','rgba(255,255,255,0.85)']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                We offer finance and consulting
                                <br class="responsive">services for companies &
                                <br class="desktop">businesses
                                <br class="responsive"> working in these industries.
                             </div>

                            <!-- LAYER NR. 4 -->
                            <a class="tp-caption btn btn-primary general_btn "
                                 id="slide-10-layer-20"
                                 data-x="['left','left','left','left']" data-hoffset="['15','15','20','10']"
                                 data-y="['top','top','middle','middle']" data-voffset="['785','760','210','205']"
                                 data-fontsize="['16','16','12','13']"
                                 data-lineheight="['16','16','16','12']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-color="#ffffff"
                                 data-type="button"
                                 data-responsive_offset="on"
                                 data-responsive="off"
                                 data-frames='[{"delay":2000,"speed":2500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[20,20,10,10]"
                                 data-paddingright="[38,38,15,15]"
                                 data-paddingbottom="[20,20,10,10]"
                                 data-paddingleft="[38,38,15,15]">
                                 Get Started
                             </a>

                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption hidden_excerpt tp-resizeme rs-parallaxlevel-2"
                                 id="slide-10-layer-13"
                                 data-x="['left','left','left','left']" data-hoffset="['609','609','384','256']"
                                 data-y="['top','top','top','top']" data-voffset="['250','250','300','480']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-type="image"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":2900,"speed":3300,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                <div class="rs-looped rs-wave"  data-speed="2" data-angle="0" data-radius="4px" data-origin="50% 50%">
                                    <img src="assets/images/shapes/header-slider-comment-shape.png" alt="" data-ww="['199px','199px','145px','128px']" data-hh="['111px','111px','81px','72px']" width="199" height="111" data-no-retina>
                                </div>
                            </div>

                            <!-- LAYER NR. 6 -->
                            <div class="tp-caption expert_tx  tp-resizeme rs-parallaxlevel-2"
                                 id="slide-10-layer-14"
                                 data-x="['left','left','left','left']" data-hoffset="['650','650','407','271']"
                                 data-y="['top','top','top','top']" data-voffset="['283','283','330','488']"
                                 data-fontsize="['22','22','18','19']"
                                 data-lineheight="['22','22','18','18']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-color="#ffffff"
                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":3000,"speed":3400,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                <div class="rs-looped rs-wave"  data-speed="2" data-angle="0" data-radius="4px" data-origin="50% 50%">Expert<br>Consultants </div></div>

                            <!-- LAYER NR. 7 shape-->
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-2"
                                 id="slide-10-layer-16"
                                 data-x="['left','left','left','left']" data-hoffset="['-217','-217','54','13']"
                                 data-y="['top','top','top','top']" data-voffset="['235','235','55','3']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":710,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                 <img src="assets/images/shapes/header-shape-4.png" alt="" data-ww="['30px','30px','30px','30px']" data-hh="['39px','39px','39px','39px']" width="30" height="39" data-no-retina>
                                 </div>

                            <!-- LAYER NR. 8 shape-->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                                 id="slide-10-layer-17"
                                 data-x="['left','left','left','left']" data-hoffset="['-199','-199','24','-10']"
                                 data-y="['top','top','middle','middle']" data-voffset="['760','760','252','228']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":1060,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                 <img src="assets/images/shapes/header-shape-3.png" alt="" data-ww="['109px','109px','109px','109px']" data-hh="['85px','85px','85px','85px']" width="109" height="85" data-no-retina>
                             </div>

                            <!-- LAYER NR. 9 shape-->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                                 id="slide-10-layer-18"
                                 data-x="['left','left','left','left']" data-hoffset="['593','593','666','418']"
                                 data-y="['top','top','middle','middle']" data-voffset="['844','844','-222','-229']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                 <img src="assets/images/shapes/header-shape-2.png" alt="" data-ww="['43px','43px','43px','43px']" data-hh="['77px','77px','77px','77px']" width="43" height="77" data-no-retina>
                             </div>

                            <!-- LAYER NR. 10 shape-->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                                 id="slide-10-layer-19"
                                 data-x="['right','right','right','right']" data-hoffset="['-272','-272','263','179']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['403','403','-2','-78']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":1760,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                 <img src="assets/images/shapes/header-shape-1.png" alt="" data-ww="['290px','290px','181px','181px']" data-hh="['228px','228px','142px','142px']" width="290" height="228" data-no-retina>
                                 </div>

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption header_man_img_1 tp-resizeme "
                                 id="slide-10-layer-12"
                                 data-x="['right','right','right','right']" data-hoffset="['25','-1','20','-30']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['-15','-15','-1','-5']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":2500,"speed":3000,"frame":"0","from":"y:100px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                 <img src="assets/images/header-slider-img-2.png" alt="" data-ww="['455px','455px','279px','196px']" data-hh="['826px','826px','506px','357px']" width="455" height="826" data-no-retina>
                             </div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-index="rs-11" data-transition="fade,random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="300,default,default,default"  data-rotate="0,0,0,0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="assets/images/sliders/slider-1.jpg"  alt="" title="slider-1"  width="1894" height="1043" data-bgposition="center center" data-kenburns="on" data-duration="8000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="9" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption  yellow_tx tp-resizeme"
                                 id="slide-11-layer-10"
                                 data-x="['left','left','left','left']" data-hoffset="['15','15','20','10']"
                                 data-y="['top','top','top','top']" data-voffset="['371','341','300','260']"
                                 data-fontsize="['26','26','20','22']"
                                 data-lineheight="['24','24','22','22']"
                                 data-color="#ffed50"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[2,2,2,2]"
                                 data-paddingleft="[0,0,0,0]">
                                We Make Difference
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption big_tx  tp-resizeme"
                                 id="slide-11-layer-3"
                                 data-x="['left','left','left','left']" data-hoffset="['15','15','20','10']"
                                 data-y="['top','top','middle','middle']" data-voffset="['410','386','10','-5']"
                                 data-fontsize="['70','70','42','38']"
                                 data-lineheight="['80','80','52','40']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-color="#ffffff"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"y:-50px;opacity:0;","ease":"nothing"}]'
                                 data-textAlign="['left','left','left','left']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                Leading in finance<br>
                                and consulting<br>
                                business
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption paragraph_tx  tp-resizeme"
                                 id="slide-11-layer-21"
                                 data-x="['left','left','left','left']" data-hoffset="['15','15','20','10']"
                                 data-y="['top','top','middle','middle']" data-voffset="['676','654','125','114']"
                                 data-fontsize="['20','20','16','24']"
                                 data-lineheight="['32','32','30','32']"
                                 data-color="['rgba(255,255,255,0.7)','rgba(255,255,255,0.7)','rgba(255,255,255,0.7)','rgba(255,255,255,0.85)']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                We offer finance and consulting
                                <br class="responsive">services for companies &
                                <br class="desktop">businesses
                                <br class="responsive"> working in these industries.
                            </div>

                            <!-- LAYER NR. 4 -->
                            <a class="tp-caption btn btn-primary general_btn "
                               id="slide-11-layer-20"
                               data-x="['left','left','left','left']" data-hoffset="['15','15','20','10']"
                               data-y="['top','top','middle','middle']" data-voffset="['785','760','210','205']"
                               data-fontsize="['16','16','12','13']"
                               data-lineheight="['16','16','16','12']"
                               data-width="none"
                               data-height="none"
                               data-whitespace="nowrap"
                               data-color="#ffffff"
                               data-type="button"
                               data-responsive_offset="on"
                               data-responsive="off"
                               data-frames='[{"delay":2000,"speed":2500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
                               data-textAlign="['inherit','inherit','inherit','inherit']"
                               data-paddingtop="[20,20,10,10]"
                               data-paddingright="[38,38,15,15]"
                               data-paddingbottom="[20,20,10,10]"
                               data-paddingleft="[38,38,15,15]">
                                Get Started
                            </a>

                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption  hidden_excerpt  tp-resizeme rs-parallaxlevel-2"
                                 id="slide-11-layer-13"
                                 data-x="['left','left','left','left']" data-hoffset="['609','609','384','256']"
                                 data-y="['top','top','top','top']" data-voffset="['250','250','300','480']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":2900,"speed":3300,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                <div class="rs-looped rs-wave"  data-speed="2" data-angle="0" data-radius="4px" data-origin="50% 50%">
                                    <img src="assets/images/shapes/header-slider-comment-shape.png" alt="" data-ww="['199px','199px','145px','128px']" data-hh="['111px','111px','81px','72px']" width="199" height="111" data-no-retina>
                                </div>
                            </div>

                            <!-- LAYER NR. 6 -->
                            <div class="tp-caption expert_tx  tp-resizeme rs-parallaxlevel-2"
                                 id="slide-11-layer-14"
                                 data-x="['left','left','left','left']" data-hoffset="['650','650','407','271']"
                                 data-y="['top','top','top','top']" data-voffset="['283','283','330','488']"
                                 data-fontsize="['22','22','18','19']"
                                 data-lineheight="['22','22','18','18']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-color="#ffffff"
                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":3000,"speed":3400,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                <div class="rs-looped rs-wave"  data-speed="2" data-angle="0" data-radius="4px" data-origin="50% 50%">Expert<br>Consultants </div></div>

                            <!-- LAYER NR. 7 shape-->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                                 id="slide-11-layer-16"
                                 data-x="['left','left','left','left']" data-hoffset="['-217','-217','54','13']"
                                 data-y="['top','top','top','top']" data-voffset="['235','235','55','3']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":710,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"><img src="assets/images/shapes/header-shape-4.png" alt="" data-ww="['30px','30px','30px','30px']" data-hh="['39px','39px','39px','39px']" width="30" height="39" data-no-retina> </div>

                            <!-- LAYER NR. 8 shape-->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                                 id="slide-11-layer-17"
                                 data-x="['left','left','left','left']" data-hoffset="['-199','-199','24','-10']"
                                 data-y="['top','top','middle','middle']" data-voffset="['760','760','252','228']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":1060,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                <img src="assets/images/shapes/header-shape-3.png" alt="" data-ww="['109px','109px','109px','109px']" data-hh="['85px','85px','85px','85px']" width="109" height="85" data-no-retina>
                            </div>

                            <!-- LAYER NR. 9 shape-->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                                 id="slide-11-layer-18"
                                 data-x="['left','left','left','left']" data-hoffset="['593','593','666','418']"
                                 data-y="['top','top','middle','middle']" data-voffset="['844','844','-222','-229']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                <img src="assets/images/shapes/header-shape-2.png" alt="" data-ww="['43px','43px','43px','43px']" data-hh="['77px','77px','77px','77px']" width="43" height="77" data-no-retina>
                            </div>

                            <!-- LAYER NR. 10 shape-->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                                 id="slide-11-layer-19"
                                 data-x="['right','right','right','right']" data-hoffset="['-272','-272','263','179']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['403','403','-2','-78']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":1760,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                <img src="assets/images/shapes/header-shape-1.png" alt="" data-ww="['290px','290px','181px','181px']" data-hh="['228px','228px','142px','142px']" width="290" height="228" data-no-retina>
                            </div>

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption  header_man_img_1 tp-resizeme "
                                 id="slide-11-layer-12"
                                 data-x="['right','right','right','right']" data-hoffset="['80','-1','20','-30']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['-15','-15','-1','-5']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="image"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":2500,"speed":3000,"frame":"0","from":"y:100px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]">
                                <img src="assets/images/header-slider-img-2.png" alt="" data-ww="['455px','455px','279px','196px']" data-hh="['826px','826px','506px','357px']" width="455" height="826" data-no-retina>
                            </div>
                        </li>
                        <!-- SLIDE  -->
                    </ul>
                </div><!--end .rev_slider.rev_slider_display-->
            </div><!-- END REVOLUTION SLIDER -->


        </article>
    </section>
    <!-- ***************************
        Header Slider Area End
     *************************** -->

    <!-- ***************************
        Welcome Area Start
     *************************** -->
     <section class="welcome_area section_padding section_bb">
         <div class="container">
             <div class="row">
                 <div class="col-lg-6">
                     <div class="welcome_left">
                         <p class="year_box">25+ years of experience</p>
                         <img src="assets/images/wlc-1.jpg" alt="Welcome Image">
                         <img class="right_img" src="assets/images/wlc-2.jpg" alt="Welcome Image">
                     </div><!--end .welcome_left-->
                 </div><!--end .col-lg-6-->
                 <div class="col-lg-6">
                     <div class="welcome_right">
                         <div class="hero_section_title mb_40">
                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                                 <g>
                                     <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                     <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                                 </g>
                             </svg>
                             <h4>Welcome To Vizeon</h4>
                             <h1>Better solution at your fingertips</h1>
                         </div><!--end .hero_section_title-->
                         <p>
                             There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.
                         </p>
                         <a href="#" class="btn btn-primary general_btn">Discover More</a>
                     </div><!--end .welcome_right-->
                 </div><!--end .col-lg-6-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .welcome_area-->
    <!-- ***************************
        Welcome Area End
     *************************** -->

    <!-- ***************************
        Icon Box Area Start
     *************************** -->
     <div class="icon_box_area section_padding">
         <div class="container">
             <div class="row">
                 <div class="col-lg-3 col-md-6">
                     <div class="icon_box">
                         <i class="icon-pie-graphic"></i>
                         <p>World leader in consulting and finance</p>
                     </div><!--end .icon_box-->
                 </div><!--end .col-lg-3 col-md-6-->
                 <div class="col-lg-3 col-md-6">
                     <div class="icon_box">
                         <i class="icon-human-resources"></i>
                         <p>A focused team with a specialized skill set</p>
                     </div><!--end .icon_box-->
                 </div><!--end .col-lg-3 col-md-6-->
                 <div class="col-lg-3 col-md-6">
                     <div class="icon_box">
                         <i class="icon-options"></i>
                         <p>Trusted and professional advisors for you</p>
                     </div><!--end .icon_box-->
                 </div><!--end .col-lg-3 col-md-6-->
                 <div class="col-lg-3 col-md-6">
                     <div class="icon_box">
                         <i class="icon-training"></i>
                         <p>Experience to give you a better results</p>
                     </div><!--end .icon_box-->
                 </div><!--end .col-lg-3 col-md-6-->
             </div><!--end .row-->
         </div><!--end .container-->
     </div><!--end .icon_box_area-->
    <!-- ***************************
        Icon Box Area End
     *************************** -->

    <!-- ***************************
        Our Service Area Start
     *************************** -->
     <section class="service_area section_padding section_bg section_bb">
         <div class="container">
             <div class="row">
                 <div class="col-lg-8 offset-lg-2">
                     <div class="hero_section_title text-center">
                         <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                             <g>
                                 <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                 <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                             </g>
                         </svg>
                         <h4>Our All Services</h4>
                         <h1>What we are offering to customers</h1>
                     </div><!--end .hero_section_title-->
                 </div><!--end .col-lg-8-->
             </div><!--end .row-->
             <div class="row">
                 <div class="col-lg-12">
                     <div class="service_slider_box service_slider owl-carousel">
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                 <div class="details_bg">
                                     <h2>
                                         <a href="service-single.blade.php">
                                             Marketing
                                             <br>Strategy
                                         </a>
                                     </h2>
                                     <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                         <i class="fa fa-angle-right"></i>
                                     </a>
                                 </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                <div class="details_bg">
                                    <h2>
                                        <a href="service-single.blade.php">
                                            Audit
                                            <br>marketing
                                        </a>
                                    </h2>
                                    <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                <div class="details_bg">
                                    <h2>
                                        <a href="service-single.blade.php">
                                            Risk
                                            <br>Management
                                        </a>
                                    </h2>
                                    <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                <div class="details_bg">
                                    <h2>
                                        <a href="service-single.blade.php">
                                            Marketing <br>Strategy
                                        </a>
                                    </h2>
                                    <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                 <div class="details_bg">
                                     <h2>
                                         <a href="service-single.blade.php">
                                             Audit <br>marketing
                                         </a>
                                     </h2>
                                     <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                         <i class="fa fa-angle-right"></i>
                                     </a>
                                 </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                <div class="details_bg">
                                    <h2>
                                        <a href="service-single.blade.php">
                                            Risk <br>Management
                                        </a>
                                    </h2>
                                    <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                <div class="details_bg">
                                    <h2>
                                        <a href="service-single.blade.php">
                                            Marketing <br>Strategy
                                        </a>
                                    </h2>
                                    <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                <div class="details_bg">
                                    <h2>
                                        <a href="service-single.blade.php">
                                            Audit <br>marketing
                                        </a>
                                    </h2>
                                    <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                         <div class="service_slider_details">
                             <img src="assets/images/services/sv-1.jpg" alt="Service Image">
                             <div class="details_box">
                                <div class="details_bg">
                                    <h2>
                                        <a href="service-single.blade.php">
                                            Risk <br>Management
                                        </a>
                                    </h2>
                                    <a href="service-single.blade.php" class="btn btn-primary general_btn">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div><!--end .details_bg-->
                             </div><!--end .details_box-->
                         </div><!--end .service_slider_details-->
                     </div><!--end .service_slider_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .service_area-->
    <!-- ***************************
        Our Service Area End
     *************************** -->

    <!-- ***************************
        Newsletter Area Start
     *************************** -->
     <section class="newsletter_area section_padding">
         <div class="container">
             <div class="row">
                 <div class="col-lg-5">
                     <h2>Don't miss our monthly updates</h2>
                 </div><!--end .col-lg-5-->
                 <div class="col-lg-7">
                     <div class="newsletter_form">
                         <form action="#" method="post">
                             <div class="input-group">
                                 <input type="email" name="EMAIL" placeholder="Enter Email">
                                 <button class="btn btn-primary general_btn" type="submit">
                                     Subscribe
                                 </button>
                             </div><!--end .input_group-->
                         </form>
                     </div><!--end .newsletter_form-->
                 </div><!--end .col-lg-7-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .newsletter_area-->
    <!-- ***************************
        Newsletter Area End
     *************************** -->

    <!-- ***************************
        Video Promotion Area Start
     *************************** -->
     <section class="video_promotion_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-8 offset-lg-2">
                     <div class="video_promotion_details">
                         <a href="https://www.w3schools.com/html/mov_bbb.mp4" class="lightbox" title="Watch Video">
                             <i class="fa fa-play"></i>
                         </a>
                         <p>Finance and Consulting Business</p>
                         <h1>Our techinical expertise across the market</h1>
                     </div><!--end .video_promotion_details-->
                 </div><!--end .col-lg-8.offset-lg-2-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .video_promotion_area-->
    <!-- ***************************
        Video Promotion Area End
     *************************** -->

    <!-- ***************************
        Trusted Area Start
     *************************** -->
     <section class="trusted_area section_padding">
         <div class="container">
             <div class="row">
                 <div class="col-lg-7">
                     <div class="trusted_left_details">
                         <div class="hero_section_title mb_45">
                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                                 <g>
                                     <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                     <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                                 </g>
                             </svg>
                             <h4>Trusted & Professional</h4>
                             <h1>We’re trusted by more than <strong>84,106</strong> clients</h1>
                         </div><!--end .hero_section_title-->
                         <img src="assets/images/trusted-1.jpg" alt="Trusted Image">
                         <div class="trusted_sample_tx">
                             We only choose the best one for you
                         </div><!--en d.trusted_sample_tx-->
                     </div><!--end .trusted_left_details-->
                 </div><!--end .col-lg-7-->
                 <div class="col-lg-5">
                     <div class="trusted_right text-right">
                         <img src="assets/images/trusted-2.jpg" alt="Trusted Image">
                     </div><!--end .trusted_right-->
                 </div><!--end .col-lg-5-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .trusted_area-->
    <!-- ***************************
        Trusted Area End
     *************************** -->

    <!-- ***************************
        Team Member Area Start
     *************************** -->
     <section class="team_member_area">
         <div class="container">
             <div class="row">
                 <div class="col-lg-8 offset-lg-2">
                     <div class="hero_section_title mb_45 text-center">
                         <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                             <g>
                                 <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                 <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                             </g>
                         </svg>
                         <h4>Meet The Team</h4>
                         <h1>Meet our professional team members</h1>
                     </div><!--end .hero_section_title-->
                 </div><!--end .col-lg-8.offset-lg-2-->
             </div><!--end .row-->
             <div class="row">
                 <div class="col-lg-3 col-md-6">
                     <div class="team_box">
                         <div class="img_box">
                             <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                             <ul class="overlay">
                                 <li>
                                     <a href="#">Twitter</a>
                                 </li>
                                 <li>
                                     <a href="#">Facebook</a>
                                 </li>
                                 <li>
                                     <a href="#">Linkedin</a>
                                 </li>
                             </ul>
                         </div><!--end .img_box-->
                         <div class="team_details_box">
                             <div class="details_bg text-center">
                                 <h3>Christine Eve</h3>
                                 <p>Officer</p>
                             </div><!--end .details_bg-->
                         </div><!--end .team_details_box-->
                     </div><!--end .team_box-->
                 </div><!--end .col-lg-3-->
                 <div class="col-lg-3 col-md-6">
                     <div class="team_box">
                         <div class="img_box">
                             <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                             <ul class="overlay">
                                 <li>
                                     <a href="#">Twitter</a>
                                 </li>
                                 <li>
                                     <a href="#">Facebook</a>
                                 </li>
                                 <li>
                                     <a href="#">Linkedin</a>
                                 </li>
                             </ul>
                         </div><!--end .img_box-->
                         <div class="team_details_box">
                             <div class="details_bg text-center">
                                 <h3>Kevin Smith</h3>
                                 <p>Consultant</p>
                             </div><!--end .details_bg-->
                         </div><!--end .team_details_box-->
                     </div><!--end .team_box-->
                 </div><!--end .col-lg-3-->
                 <div class="col-lg-3 col-md-6">
                     <div class="team_box">
                         <div class="img_box">
                             <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                             <ul class="overlay">
                                 <li>
                                     <a href="#">Twitter</a>
                                 </li>
                                 <li>
                                     <a href="#">Facebook</a>
                                 </li>
                                 <li>
                                     <a href="#">Linkedin</a>
                                 </li>
                             </ul>
                         </div><!--end .img_box-->
                         <div class="team_details_box">
                             <div class="details_bg text-center">
                                 <h3>Jessica Brown</h3>
                                 <p>Director</p>
                             </div><!--end .details_bg-->
                         </div><!--end .team_details_box-->
                     </div><!--end .team_box-->
                 </div><!--end .col-lg-3-->
                 <div class="col-lg-3 col-md-6">
                     <div class="team_box">
                         <div class="img_box">
                             <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                             <ul class="overlay">
                                 <li>
                                     <a href="#">Twitter</a>
                                 </li>
                                 <li>
                                     <a href="#">Facebook</a>
                                 </li>
                                 <li>
                                     <a href="#">Linkedin</a>
                                 </li>
                             </ul>
                         </div><!--end .img_box-->
                         <div class="team_details_box">
                             <div class="details_bg text-center">
                                 <h3>Mark Hardson</h3>
                                 <p>Manager</p>
                             </div><!--end .details_bg-->
                         </div><!--end .team_details_box-->
                     </div><!--end .team_box-->
                 </div><!--end .col-lg-3-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .team_member_area-->
    <!-- ***************************
        Team Member Area End
     *************************** -->

    <!-- ***************************
        Professionals Area Start
     *************************** -->
     <section class="professional_area section_padding section_bg">
         <div class="container">
             <div class="row">
                 <div class="col-lg-4 col-md-6">
                     <div class="professional_left">
                         <div class="hero_section_title mb_60">
                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                                 <g>
                                     <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                     <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                                 </g>
                             </svg>
                             <h4>We're Professionals</h4>
                             <h1>Get the best consultancy from your expert advisors</h1>
                         </div><!--end .hero_section_title-->
                         <a href="#" class="btn btn-primary general_btn">Learn More</a>
                     </div><!--end .professional_left-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="professional_middle">
                         <p>Tincidunt elit magnis nulla facl sag ttis free maecenas. Sapien lorem ipsum is simply free text available in the market to use in the site tat youy can use it easily nuned amet trices dolores sit ipsum velit purus aliquet massa fring illa leo orci. </p>
                     </div><!--end .professional_middle-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="professional_right" id="progress_bar">
                         <div class="skills">
                             <div class="skill">
                                 <h3 class="skill__title">Finance</h3>
                                 <span>88%</span>
                                 <div class="progress_bg">
                                     <div class="progress_bar"></div>
                                 </div><!--end .progress_bg-->
                             </div><!--end .skill-->
                             <div class="skill">
                                 <h3 class="skill__title">Consultancy</h3>
                                 <span>34%</span>
                                 <div class="progress_bg">
                                     <div class="progress_bar"></div>
                                 </div><!--end .progress_bg-->
                             </div><!--end .skill-->
                             <div class="skill">
                                 <h3 class="skill__title">Business</h3>
                                 <span>66%</span>
                                 <div class="progress_bg">
                                     <div class="progress_bar"></div>
                                 </div><!--end .progress_bg-->
                             </div><!--end .skill-->
                         </div><!-- end skills-->
                     </div><!--end .professional_right-->
                 </div><!--end .col-lg-4-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .professional_area-->
    <!-- ***************************
        Professionals Area End
     *************************** -->

    <!-- ***************************
        CounterUp Area Start
     *************************** -->
     <section class="counterup_area section_padding light_bg">
         <img class="counter_bg_right" src="assets/images/counter-bg.jpg" alt="Counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 pr_0">
                    <div class="counter_box mb_60 pb_55">
                        <i class="icon-happiness"></i>
                        <div class="counter_details">
                            <h1 class="counterup">784,000</h1>
                            <p>Customers are happy with our services</p>
                        </div><!--end .counter_details-->
                    </div><!--end .counter_box-->
                    <div class="counter_box pb_55">
                        <i class="icon-project"></i>
                        <div class="counter_details">
                            <h1 class="counterup">46,000</h1>
                            <p>Projects are completed by vizeon</p>
                        </div><!--end .counter_details-->
                    </div><!--end .counter_box-->
                </div><!--end .col-lg-5.pr_0-->
            </div><!--end .row-->
        </div><!--end .container-->
     </section><!--end .counterup_area-->
    <!-- ***************************
        CounterUp Area End
     *************************** -->

    <!-- ***************************
        Latest Blog Area Start
     *************************** -->
     <section class="blog_area responsive_pb section_bb">
         <div class="container">
             <div class="row">
                 <div class="col-lg-6 offset-lg-3 text-center">
                     <div class="hero_section_title mb_45">
                         <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                             <g>
                                 <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                 <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                             </g>
                         </svg>
                         <h4>Latest Blog Posts</h4>
                         <h1>Recent News & Articles</h1>
                     </div><!--end .hero_section_title-->
                 </div><!--end .col-lg-6-->
             </div><!--end .row-->
             <div class="row">
                 <div class="col-lg-4 col-md-6">
                     <div class="blog_box">
                         <div class="img_box">
                             <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                             <div class="post_meta">
                                 <p class="admin">By <a href="#">Admin</a></p>
                                 <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">4</a></p>
                             </div><!--end .post_meta-->
                         </div><!--end .img_box-->
                         <div class="blog_details">
                             <div class="date">27 March, 2019</div>
                             <h2>
                                 <a href="blog-single.blade.php">Many important brands have given us their trust</a>
                                 </h2>
                                 <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                             <a class="readmore" href="blog-single.blade.php">Read More</a>
                         </div><!--end .blog_details-->
                     </div><!--end .blog_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="blog_box">
                         <div class="img_box">
                             <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                             <div class="post_meta">
                                 <p class="admin">By <a href="#">Admin</a></p>
                                 <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">2</a></p>
                             </div><!--end .post_meta-->
                         </div><!--end .img_box-->
                         <div class="blog_details">
                             <div class="date">25 March, 2019</div>
                             <h2>
                                 <a href="blog-single.blade.php">Future where technology creates good jobs</a>
                                 </h2>
                                 <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                             <a class="readmore" href="blog-single.blade.php">Read More</a>
                         </div><!--end .blog_details-->
                     </div><!--end .blog_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="blog_box">
                         <div class="img_box">
                             <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                             <div class="post_meta">
                                 <p class="admin">By <a href="#">Admin</a></p>
                                 <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">0</a></p>
                             </div><!--end .post_meta-->
                         </div><!--end .img_box-->
                         <div class="blog_details">
                             <div class="date">20 March, 2019</div>
                             <h2>
                                 <a href="blog-single.blade.php">What you do today can improve all your tomorrows</a>
                                 </h2>
                                 <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                             <a class="readmore" href="blog-single.blade.php">Read More</a>
                         </div><!--end .blog_details-->
                     </div><!--end .blog_box-->
                 </div><!--end .col-lg-4-->

             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .blog_area-->
    <!-- ***************************
        Latest Blog Area End
     *************************** -->

    <!-- ***************************
        Client Area Start
     *************************** -->
    <div class="client_logo_area section_padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="client_slider_section client_slider owl-carousel">
                        <div class="client_img_box">
                            <img src="assets/images/client-logo.png" alt="Client Image">
                        </div><!--end .client_img_box-->
                        <div class="client_img_box">
                            <img src="assets/images/client-logo.png" alt="Client Image">
                        </div><!--end .client_img_box-->
                        <div class="client_img_box">
                            <img src="assets/images/client-logo.png" alt="Client Image">
                        </div><!--end .client_img_box-->
                        <div class="client_img_box">
                            <img src="assets/images/client-logo.png" alt="Client Image">
                        </div><!--end .client_img_box-->
                        <div class="client_img_box">
                            <img src="assets/images/client-logo.png" alt="Client Image">
                        </div><!--end .client_img_box-->
                        <div class="client_img_box">
                            <img src="assets/images/client-logo.png" alt="Client Image">
                        </div><!--end .client_img_box-->
                    </div><!--end .client_slider_section-->
                </div><!--end .col-md-12-->
            </div><!--end .row-->
        </div><!--end .container-->
    </div><!--end .client_logo_area-->
    <!-- ***************************
        Client Area End
     *************************** -->

    <!-- ***************************
        Google Map Area Start
     *************************** -->
    <div id="google_map">
        <div id="map"></div>
    </div><!--end .google_map-->
    <!-- ***************************
        Google Map Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->

    @include ('layouts.user.footer')

     <!-- ***************************
        Footer Area End
     *************************** -->

     @include ('layouts.user.script.indexScript')

</body>
</html>