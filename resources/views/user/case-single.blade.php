<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.lightboxStyle')
    
</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Our Cases</li>
                             </ol>
                         </nav>
                         <h1>Our cases</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        Cases Single Area Start
     *************************** -->
     <section class="single_case_area section_padding">
         <div class="container">
             <div class="row">
                 <div class="col-lg-8 col-md-7">
                     <div class="left">
                     </div><!--end .left-->
                 </div><!--end .col-lg-8-->
                 <div class="col-lg-4 col-md-5">
                     <div class="right">
                     </div><!--end .right-->
                 </div><!--end .col-lg-4-->
             </div><!--end .row-->
             <div class="row mt_50">
                 <div class="col-lg-4 col-md-4">
                     <div class="case_sidebar">
                         <div class="project_address">
                             <h4>Project Name:</h4>
                             <span>Business Accounting</span>
                             <h4>Client:</h4>
                             <span>Company Name inc:</span>
                             <h4>Project Commencement Date:</h4>
                             <span>January 26, 2018</span>
                             <h4>Project Completion Date:</h4>
                             <span>March 27, 2019</span>
                             <h4>Project Url:</h4>
                             <span><a href="#">www.example.com</a></span>
                         </div><!--end .project_address-->
                         <div class="challenge_box">
                             <h2>Business Challenges</h2>
                             <ul>
                                 <li><i class="fa fa-check"></i> Research beyond the business plan</li>
                                 <li><i class="fa fa-check"></i> Marketing options and rates</li>
                                 <li><i class="fa fa-check"></i> The ability to turnaround consulting</li>
                                 <li><i class="fa fa-check"></i> Help companies into more profitable</li>
                                 <li><i class="fa fa-check"></i> Customer engagement matters</li>
                             </ul>
                         </div><!--end .challenge_box-->
                     </div><!--end .case_sidebar-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-8 col-md-8">
                    <div class="project_content">
                        <p>Need something changed or is there something not quite working the way you envisaged? Is your van a little old and tired and need refreshing? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
                        <h2>Challenge & Solutions</h2>
                        <p>Neque porro est qui dolorem ipsum quia quaed inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit amet finibus eros. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ndustry stan when an unknown printer took a galley.</p>
                        <h3><i class="icon-develop"></i> Attract and retian high quality paying customers</h3>
                        <p class="pg_2">When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.</p>
                        <h3 class="sub_mg"><i class="icon-training-2"></i> Strong business plan requires experience</h3>
                        <p class="pg_3">When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ndustry stan when an unknown printer took a galley.</p>
                    </div><!--end .project_content-->
                 </div><!--end .col-lg-8-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .single_case_area-->
    <!-- ***************************
        Cases Single Area End
     *************************** -->

    <!-- ***************************
        Helping Over Area Start
     *************************** -->
    <section class="helping_over_area section_padding light_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <h1>Helping over <strong>786,000</strong> businesses in USA</h1>
                </div><!--end .col-lg-9-->
                <div class="col-lg-3 col-md-4 text-right">
                    <a href="#" class="btn btn-primary general_btn">Get Started</a>
                </div><!--end .col-lg-3-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .helping_over_area-->
    <!-- ***************************
        Helping Over Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
     <!-- ***************************
        Footer Area End
     *************************** -->



     @include ('layouts.user.script.lightboxScript')

</body>
</html>