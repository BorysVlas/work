<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.lightboxStyle')
</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Our Cases</li>
                             </ol>
                         </nav>
                         <h1>Our cases</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        Our Cases Area Start
     *************************** -->
    <section class="cases_area grid_cases section_bb">
        <div class="container">
            <div class="row row_margin">
                <div class="col-lg-6 col-md-6">
                    <div class="case_box">
                        <div class="case_details">
                            <i class="icon-growth"></i>
                            <div class="right">
                                <h3>Financial Marketing Advice</h3>
                                <a href="case-single.html">Read More</a>
                            </div><!--end .right-->
                        </div><!--end .case_details-->
                    </div><!--end .case_box-->
                </div><!--end .col-lg-6-->
                <div class="col-lg-6 col-md-6">
                    <div class="case_box bg_2">
                        <div class="case_details">
                            <i class="icon-goal"></i>
                            <div class="right">
                                <h3>Research Beyond the Business</h3>
                                <a href="case-single.html">Read More</a>
                            </div><!--end .right-->
                        </div><!--end .case_details-->
                    </div><!--end .case_box-->
                </div><!--end .col-lg-6-->
            </div><!--end .row-->
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="case_box bg_3">
                        <div class="case_details">
                            <i class="icon-achievement"></i>
                            <div class="right">
                                <h3>Substantial Business Growth</h3>
                                <a href="case-single.html">Read More</a>
                            </div><!--end .right-->
                        </div><!--end .case_details-->
                    </div><!--end .case_box-->
                </div><!--end .col-lg-6-->
                <div class="col-lg-6 col-md-6">
                    <div class="case_box bg_4">
                        <div class="case_details">
                            <i class="icon-strategy"></i>
                            <div class="right">
                                <h3>Build Digital Compaign Brands</h3>
                                <a href="case-single.html">Read More</a>
                            </div><!--end .right-->
                        </div><!--end .case_details-->
                    </div><!--end .case_box-->
                </div><!--end .col-lg-6-->
            </div><!--end .row-->
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="case_box bg_5">
                        <div class="case_details">
                            <i class="icon-training-2"></i>
                            <div class="right">
                                <h3>Developing Paperless Practice</h3>
                                <a href="case-single.html">Read More</a>
                            </div><!--end .right-->
                        </div><!--end .case_details-->
                    </div><!--end .case_box-->
                </div><!--end .col-lg-6-->
                <div class="col-lg-6 col-md-6">
                    <div class="case_box bg_6">
                        <div class="case_details">
                            <i class="icon-develop"></i>
                            <div class="right">
                                <h3>Businesses Large and Small</h3>
                                <a href="case-single.html">Read More</a>
                            </div><!--end .right-->
                        </div><!--end .case_details-->
                    </div><!--end .case_box-->
                </div><!--end .col-lg-6-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .cases_area-->
    <!-- ***************************
        Our Cases Area End
     *************************** -->

    <!-- ***************************
        CTA Area Start
     *************************** -->
    <section class="cta_area section_padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="hero_section_title mb_60">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                            <g>
                                <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                            </g>
                        </svg>
                        <h4>We’re Professional and Expert Consultants</h4>
                        <h1>For better help and business. <span>Let’s get started</span></h1>
                    </div><!--end .hero_section_title-->
                    <a href="#" class="btn btn general_btn">Get Started</a>
                </div><!--end .col-lg-10.offset-lg-1-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .cta_area-->
    <!-- ***************************
        CTA Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
     <!-- ***************************
        Footer Area End
     *************************** -->



     @include ('layouts.user.script.lightboxScript')


</body>
</html>