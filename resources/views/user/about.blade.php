<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.owlStyle')
    


</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
    @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Pages</li>
                                 <li class="breadcrumb-item active" aria-current="page">About Us</li>
                             </ol>
                         </nav>
                         <h1>About Us</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        About Company Area Start
     *************************** -->
    <section class="about_company_area extra_padding section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="hero_section_title mb_60 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                            <g>
                                <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                            </g>
                        </svg>
                        <h4>About Our Consultancy Company</h4>
                        <h1>It just might inspire you
                            <br>to help us build a better future</h1>
                    </div><!--end .hero_section_title-->
                </div><!--end .col-lg-10.offset-lg-1-->
            </div><!--end .row-->
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="img_box"></div>
                </div><!--end .col-lg-4-->
                <div class="col-lg-4 col-md-6">
                    <div class="img_box bg_2"></div>
                </div><!--end .col-lg-4-->
                <div class="col-lg-4 col-md-6">
                    <div class="img_box bg_3"></div>
                </div><!--end .col-lg-4-->
            </div><!--end .row-->
            <div class="row">
                <div class="details text-center">
                    <p>
                        Lorem ipsum dolor sit amet nsectetur cing elit. Suspe ndisse suscipit sagittis leo sit met entum is not estibulum dignissim posuere cubilia durae. Leo sit met entum cubilia crae. but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages and more recently.
                    </p>
                    <a href="#"
                       class="btn btn-primary general_btn">Discover More</a>
                </div><!--end .col-lg-4-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .about_company_area-->
    <!-- ***************************
        About Company Area End
     *************************** -->

    <!-- ***************************
        Advice Area Start
     *************************** -->
    <section class="advice_area section_padding light_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="left_advice">
                        <div class="hero_section_title">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                                <g>
                                    <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                    <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                                </g>
                            </svg>
                            <h4>Get a Professional Advice</h4>
                            <h1>Think ahead and boost your business growth</h1>
                        </div><!--end .hero_section_title-->
                    </div><!--end .left_advice-->
                </div><!--end .col-lg-6-->
                <div class="col-lg-6 col-md-6">
                    <div class="right_advice">
                        <ul>
                            <li><div><i class="fa fa-check"></i></div> We promise to respect your time</li>
                            <li><div><i class="fa fa-check"></i></div> We hire only professionals you can trust</li>
                            <li><div><i class="fa fa-check"></i></div> We promise to provide up front pricing</li>
                        </ul>
                    </div><!--end .right_advice-->
                </div><!--end .col-lg-6-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .advice_area-->
    <!-- ***************************
        Advice Area End
     *************************** -->

    <!-- ***************************
        Trusted Advisors Area Start
     *************************** -->
    <section class="trusted_advisors_area sub_padding section_padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="hero_section_title mb_60">
                        <h4>Finance and Consulting Business</h4>
                        <h1>Trusted & professional advisors for your business</h1>
                    </div><!--end .hero_section_title-->
                    <a href="#" class="btn btn-primary general_btn">Get Started</a>
                </div><!--end .col-lg-8.offset-lg-2-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .trusted_advisors_area-->
    <!-- ***************************
        Trusted Advisors Area End
     *************************** -->

    <!-- ***************************
        Team Member Area Start
     *************************** -->
    <section class="team_member_area extra_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="hero_section_title mb_45 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                            <g>
                                <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                            </g>
                        </svg>
                        <h4>Meet The Team</h4>
                        <h1>Meet our professional team members</h1>
                    </div><!--end .hero_section_title-->
                </div><!--end .col-lg-8.offset-lg-2-->
            </div><!--end .row-->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Christine Eve</h3>
                                <p>Officer</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Kevin Smith</h3>
                                <p>Consultant</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Jessica Brown</h3>
                                <p>Director</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Mark Hardson</h3>
                                <p>Manager</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .team_member_area-->
    <!-- ***************************
        Team Member Area End
     *************************** -->

    <!-- ***************************
        Testimonial Area Start
     *************************** -->
    <section class="testimonial_area section_padding section_bg text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="hero_section_title">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                            <g>
                                <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                            </g>
                        </svg>
                        <h4>Our Testimonials</h4>
                        <h1>What they are talking about us</h1>
                    </div><!--end .hero_section_title-->
                </div><!--end .col-lg-8.offset-lg-2-->
            </div><!--end .row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="testimonial_slider_section testimonial_slider owl-carousel">
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Christine Eve</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Kevin Smith</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Jessica Brown</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Christine Eve</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Kevin Smith</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Jessica Brown</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Christine Eve</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Kevin Smith</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                        <div class="testimonial_slider_box">
                            <i class="fa fa-quote-left"></i>
                            <p>I was impresed by the vizeon services, lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                            <img src="assets/images/testimonials/testimonial-1.jpg" alt="Testimonial Image">
                            <h3 class="name">Jessica Brown</h3>
                            <span class="position">Customer</span>
                        </div><!--end .testimonial_slider_box-->
                    </div><!--end .testimonial_slider_section-->
                </div><!--end .col-lg-12-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .testimonial_area-->
    <!-- ***************************
        Testimonial Area End
     *************************** -->

    <!-- ***************************
        CTA Area Start
     *************************** -->
    <section class="cta_area section_padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="hero_section_title mb_60">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                            <g>
                                <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                            </g>
                        </svg>
                        <h4>We’re Professional and Expert Consultants</h4>
                        <h1>For better help and business. <span>Let’s get started</span></h1>
                    </div><!--end .hero_section_title-->
                    <a href="#" class="btn btn general_btn">Get Started</a>
                </div><!--end .col-lg-10.offset-lg-1-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .cta_area-->
    <!-- ***************************
        CTA Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
     <!-- ***************************
        Footer Area End
     *************************** -->



     @include ('layouts.user.script.owlScript')
    

</body>
</html>