<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.owlStyle')


</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Blog</li>
                             </ol>
                         </nav>
                         <h1>Blog posts</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        Blog Single Area Start
     *************************** -->
    <section class="blog_single_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog_content_left">
                        <div class="img_box">
                            <div class="post_meta">
                                <p class="admin">By <a href="#">Admin</a></p>
                                <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">2</a></p>
                            </div><!--end .post_meta-->
                        </div><!--end .img_box-->
                        <span class="date">25 March, 2019</span>
                        <h1>Future where technology creates good jobs</h1>
                        <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros. Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit amet finibus eros. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ndustry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. Lorem Ipsum is simply dummy text of the new design printng and type setting Ipsum Take a look at our round up of the best shows coming soon to your telly box has been the is industrys. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has industr standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of.</p>
                        <blockquote>
                            <p>“Long established fact that a reader will be distracted by the readable content of a page”</p>
                        </blockquote>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. </p>
                        <div class="tags">
                            <p>Tags: <a href="#">Consulting,</a> <a href="#">Business,</a> <a href="#">Advice</a></p>
                        </div><!--end .tags-->


                        <div class="blog_comment_box">
                            <div class="title-box">
                                <h3>2 comments</h3>
                            </div><!--end .title-box-->
                            <div id="comments" class="comments-area">
                                <ul class="comment-list">
                                    <li class="comment even thread-even depth-1 new-depth" >
                                        <div class="single-comment-box">
                                            <div class="comment_image">
                                                <img src="assets/images/blogs/comment-1.jpg" class="avatar avatar-100 photo" alt="comment avatar">
                                            </div><!--end .comment_image-->
                                            <div class="text-box">
                                                <h3>Kevin Martin <span class="comment_date_time">26 Mar, 2019</span></h3>
                                                <div class="comment-meta comment-title">
                                                    <p>Lorem Ipsum is simply dummy text of the rinting and typesetting been the industry standard dummy text ever sincer condimentum purus. In non ex at ligula fringilla lobortis.</p>
                                                </div><!--end .comment-meta-->
                                                <div class="reply comment-title">
                                                    <a rel="nofollow" class="btn-gray comment-reply-link btn btn-default default_btn" href="#">Reply</a>
                                                </div><!--end .reply .comment-title-->
                                            </div><!--end .text-box-->
                                        </div><!--end .single-comment-box-->
                                    </li><!-- #comment-## -->
                                    <li class="comment even thread-even depth-1 new-depth">
                                        <div class="single-comment-box">
                                            <div class="comment_image">
                                                <img src="assets/images/blogs/comment-1.jpg" class="avatar avatar-100 photo" alt="comment avatar">
                                            </div><!--end .comment_image-->
                                            <div class="text-box">
                                                <h3>Jessica Brown <span class="comment_date_time">26 Mar, 2019</span></h3>
                                                <div class="comment-meta comment-title">
                                                    <p>Lorem Ipsum is simply dummy text of the rinting and typesetting been the industry standard dummy text ever sincer condimentum purus. In non ex at ligula fringilla lobortis.</p>
                                                </div><!--end .comment-meta-->
                                                <div class="reply comment-title">
                                                    <a rel="nofollow" class="comment-reply-link" href="#">Reply</a>
                                                </div><!--end .reply .comment-title-->
                                            </div><!--end .text-box-->
                                        </div><!--end .single-comment-box-->
                                    </li><!-- #comment-## -->
                                </ul>

                                <div id="respond" class="comment-respond">
                                    <div class="title-box">
                                        <h3>Leave a Comment</h3>
                                    </div><!--end .title-box-->
                                    <form action="#" method="post">
                                        <div class="row">
                                            <div class="col-md-6 pr_10">
                                                <div class="input-group">
                                                    <input type="text" placeholder="Your name">
                                                </div><!--end .input-group-->
                                            </div><!--end .col-md-6-->
                                            <div class="col-md-6 pl_10">
                                                <div class="input-group">
                                                    <input type="text" placeholder="Email address">
                                                </div><!--end .input-group-->
                                            </div><!--end .col-md-6-->
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <textarea name="content" id="content" placeholder="Write message"></textarea>
                                                    <a href="#" class="btn btn-primary general_btn">Send Message</a>
                                                </div><!--end .input-group-->
                                            </div><!--end .col-md-12-->
                                        </div><!--end .row-->
                                    </form>
                                </div><!-- #respond -->
                            </div><!-- #comments -->
                        </div><!--end .blog_comment_box-->

                    </div><!--end .blog_content_left-->
                </div><!--end .col-lg-8-->
                <div class="col-lg-4">
                    <div class="blog_sidebar default_sidebar">
                        <div class="sidebar_widget author_box text-center mb_30">
                            <img src="assets/images/blogs/author.jpg" alt="Author">
                            <h3>About Author</h3>
                            <p>There a many variations of passages of lorem ipsum but the majority have suffe.</p>
                            <ul class="social_title">
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Linkedin</a></li>
                            </ul>
                        </div><!--end .sidebar_widget.author_box-->
                        <div class="sidebar_widget category mb_30">
                            <h3>Categories</h3>
                            <ul>
                                <li><a href="#">Consumer Product <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Audit Marketing <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Banking Advising <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Risk Management <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Substantial Business <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Marketing Strategy <i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div><!--end .sidebar_widget.category-->
                        <div class="sidebar_widget latest_post mb_30">
                            <h3>Latest Post</h3>
                            <ul class="posts">
                                <li>
                                    <div class="left"></div><!--end .left-->
                                    <div class="right">
                                        <span class="date">27 March, 2019</span>
                                        <a href="#">Many Important brands have give us...</a>
                                    </div><!--end .right-->
                                </li>
                                <li>
                                    <div class="left bg_2"></div><!--end .left-->
                                    <div class="right">
                                        <span class="date">20 March, 2019</span>
                                        <a href="#">What you do today can improve all your...</a>
                                    </div><!--end .right-->
                                </li>
                                <li>
                                    <div class="left bg_3"></div><!--end .left-->
                                    <div class="right">
                                        <span class="date">18 March, 2019</span>
                                        <a href="#">Money markets rates finding the best...</a>
                                    </div><!--end .right-->
                                </li>
                            </ul>
                        </div><!--end .sidebar_widget.latest_post-->
                        <div class="sidebar_widget need_help">
                            <h3>Need Help?</h3>
                            <p>Speak with a human to filling out a form? call corporate office and we will connect you with a team member who can help.</p>
                            <h2>888.333.0000</h2>
                        </div><!--end .sidebar_widget.need_help-->
                    </div><!--end .service_sidebar-->
                </div><!--end .col-lg-4-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .blog_single_area-->
    <!-- ***************************
        Blog Single Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
     <!-- ***************************
        Footer Area End
     *************************** -->



     @include ('layouts.user.script.owlScript')
    

</body>
</html>