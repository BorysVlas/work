<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.owlStyle')


</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Blog</li>
                             </ol>
                         </nav>
                         <h1>Blog sidebar</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        Latest Blog Area Start
     *************************** -->
    <section class="blog_area blog_style_2 blog_grid_style section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="blog_box">
                                <div class="img_box">
                                    <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                                    <div class="post_meta">
                                        <p class="admin">By <a href="#">Admin</a></p>
                                        <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">4</a></p>
                                    </div><!--end .post_meta-->
                                </div><!--end .img_box-->
                                <div class="blog_details">
                                    <div class="date">27 March, 2019</div>
                                    <h2>
                                        <a href="blog-single.html">Many important brands have given us their trust</a>
                                    </h2>
                                    <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                                    <a class="readmore" href="blog-single.html">Read More</a>
                                </div><!--end .blog_details-->
                            </div><!--end .blog_box-->
                        </div><!--end .col-lg-6-->
                        <div class="col-lg-6 col-md-6">
                            <div class="blog_box">
                                <div class="img_box">
                                    <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                                    <div class="post_meta">
                                        <p class="admin">By <a href="#">Admin</a></p>
                                        <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">2</a></p>
                                    </div><!--end .post_meta-->
                                </div><!--end .img_box-->
                                <div class="blog_details">
                                    <div class="date">25 March, 2019</div>
                                    <h2>
                                        <a href="blog-single.html">Future where technology creates good jobs</a>
                                    </h2>
                                    <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                                    <a class="readmore" href="blog-single.html">Read More</a>
                                </div><!--end .blog_details-->
                            </div><!--end .blog_box-->
                        </div><!--end .col-lg-6-->
                        <div class="col-lg-6 col-md-6">
                            <div class="blog_box">
                                <div class="img_box">
                                    <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                                    <div class="post_meta">
                                        <p class="admin">By <a href="#">Admin</a></p>
                                        <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">0</a></p>
                                    </div><!--end .post_meta-->
                                </div><!--end .img_box-->
                                <div class="blog_details">
                                    <div class="date">20 March, 2019</div>
                                    <h2>
                                        <a href="blog-single.html">What you do today can improve all your tomorrows</a>
                                    </h2>
                                    <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                                    <a class="readmore" href="blog-single.html">Read More</a>
                                </div><!--end .blog_details-->
                            </div><!--end .blog_box-->
                        </div><!--end .col-lg-6-->

                        <div class="col-lg-6 col-md-6">
                            <div class="blog_box">
                                <div class="img_box">
                                    <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                                    <div class="post_meta">
                                        <p class="admin">By <a href="#">Admin</a></p>
                                        <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">4</a></p>
                                    </div><!--end .post_meta-->
                                </div><!--end .img_box-->
                                <div class="blog_details">
                                    <div class="date">18 March, 2019</div>
                                    <h2>
                                        <a href="blog-single.html">Many markets rates finding the best accounts</a>
                                    </h2>
                                    <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                                    <a class="readmore" href="blog-single.html">Read More</a>
                                </div><!--end .blog_details-->
                            </div><!--end .blog_box-->
                        </div><!--end .col-lg-6-->
                        <div class="col-lg-6 col-md-6">
                            <div class="blog_box">
                                <div class="img_box">
                                    <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                                    <div class="post_meta">
                                        <p class="admin">By <a href="#">Admin</a></p>
                                        <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">2</a></p>
                                    </div><!--end .post_meta-->
                                </div><!--end .img_box-->
                                <div class="blog_details">
                                    <div class="date">16 March, 2019</div>
                                    <h2>
                                        <a href="blog-single.html">Attract and retain quality high paying customers</a>
                                    </h2>
                                    <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                                    <a class="readmore" href="blog-single.html">Read More</a>
                                </div><!--end .blog_details-->
                            </div><!--end .blog_box-->
                        </div><!--end .col-lg-6-->
                        <div class="col-lg-6 col-md-6">
                            <div class="blog_box">
                                <div class="img_box">
                                    <img src="assets/images/blogs/blog-1.jpg" alt="Blog Image">
                                    <div class="post_meta">
                                        <p class="admin">By <a href="#">Admin</a></p>
                                        <p class="comment"><i class="fa fa-comments-o"></i> <a href="#">0</a></p>
                                    </div><!--end .post_meta-->
                                </div><!--end .img_box-->
                                <div class="blog_details">
                                    <div class="date">03 March, 2019</div>
                                    <h2>
                                        <a href="blog-single.html">Solutions for all small and large business people</a>
                                    </h2>
                                    <p>Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit lorem ipsum is simply free amet finibus eros.</p>
                                    <a class="readmore" href="blog-single.html">Read More</a>
                                </div><!--end .blog_details-->
                            </div><!--end .blog_box-->
                        </div><!--end .col-lg-6-->
                    </div><!--end .row-->
                </div><!--end .col-lg-8-->
                <div class="col-lg-4">
                    <div class="blog_sidebar default_sidebar">
                        <div class="sidebar_widget author_box text-center mb_30">
                            <img src="assets/images/blogs/author.jpg" alt="Author">
                            <h3>About Author</h3>
                            <p>There a many variations of passages of lorem ipsum but the majority have suffe.</p>
                            <ul class="social_title">
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Linkedin</a></li>
                            </ul>
                        </div><!--end .sidebar_widget.author_box-->
                        <div class="sidebar_widget category mb_30">
                            <h3>Categories</h3>
                            <ul>
                                <li><a href="#">Consumer Product <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Audit Marketing <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Banking Advising <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Risk Management <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Substantial Business <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Marketing Strategy <i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div><!--end .sidebar_widget.category-->
                        <div class="sidebar_widget latest_post mb_30">
                            <h3>Latest Post</h3>
                            <ul class="posts">
                                <li>
                                    <div class="left"></div><!--end .left-->
                                    <div class="right">
                                        <span class="date">27 March, 2019</span>
                                        <a href="#">Many Important brands have give us...</a>
                                    </div><!--end .right-->
                                </li>
                                <li>
                                    <div class="left bg_2"></div><!--end .left-->
                                    <div class="right">
                                        <span class="date">20 March, 2019</span>
                                        <a href="#">What you do today can improve all your...</a>
                                    </div><!--end .right-->
                                </li>
                                <li>
                                    <div class="left bg_3"></div><!--end .left-->
                                    <div class="right">
                                        <span class="date">18 March, 2019</span>
                                        <a href="#">Money markets rates finding the best...</a>
                                    </div><!--end .right-->
                                </li>
                            </ul>
                        </div><!--end .sidebar_widget.latest_post-->
                        <div class="sidebar_widget need_help">
                            <h3>Need Help?</h3>
                            <p>Speak with a human to filling out a form? call corporate office and we will connect you with a team member who can help.</p>
                            <h2>888.333.0000</h2>
                        </div><!--end .sidebar_widget.need_help-->
                    </div><!--end .service_sidebar-->
                </div><!--end .col-lg-4-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .blog_area-->
    <!-- ***************************
        Latest Blog Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
     <!-- ***************************
        Footer Area End
     *************************** -->



     @include ('layouts.user.script.owlScript')
    

</body>
</html>