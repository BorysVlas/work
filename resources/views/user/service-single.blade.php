<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.mainStyle')



</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Our Services</li>
                             </ol>
                         </nav>
                         <h1>Our services</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        Our Service Area Start
     *************************** -->
    <section class="service_single_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="service_single_left">
                        <div class="service_single_img_box">
                            <div class="get_started_dtls">
                                <h3>We're committed to trusted financial advisors</h3>
                                <a href="#">Get Started</a>
                            </div><!--end .get_started_dtls-->
                        </div><!--end .service_single_img_box-->
                        <div class="details">
                            <h1>Audit Marketing</h1>
                            <p>Need something changed or is there something not quite working the way you envisaged? Is your van a little old and tired and need refreshing? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <div class="flex_planing">
                                <img src="assets/images/services/sv-planing.jpg" alt="Service Planing">
                                <div class="planing_box">
                                    <h3>Planning & Strategy</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <ul>
                                        <li><i class="fa fa-check"></i> Research beyond the business plan</li>
                                        <li><i class="fa fa-check"></i> Marketing options and rates</li>
                                        <li><i class="fa fa-check"></i> The ability to turnaround consulting</li>
                                        <li><i class="fa fa-check"></i> Help companies into more profitable</li>
                                        <li><i class="fa fa-check"></i> Customer engagement matters</li>
                                    </ul>
                                </div><!--end .planing_box-->
                            </div><!--end .flex_planing-->
                            <h3>Benefits of Services</h3>
                            <p>Need something changed or is there something not quite working the way you envisaged? Is your van a little old and tired and need refreshing? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the printer took a galley of type.</p>
                            <div class="service_analysis_flex">
                                <div class="left">
                                    <h3>Services Analysis</h3>
                                    <p>Sunt in culpa qui officia deserunt mollit anim est laborum. Sed perspiciatis unde omnis iste natus error sit volup tatem accusantium dolore mque laudantium, totam rem aperiam eaque ipsa quae ab illo inventore.enim ipsam voluptatem quia voluptas sit.</p>
                                    <div class="pie_chart_label">
                                        <span>one</span>
                                        <span>two</span>
                                        <span>three</span>
                                    </div><!--end. pie_chart_label-->
                                </div><!--end .left-->
                                <div class="right" id="chart">
                                    <canvas id="doughutChart" ></canvas>
                                </div><!--end .right-->
                            </div><!--end .service_analysis_flex-->
                        </div><!--end .details-->
                    </div><!--end .service_single_left-->
                </div><!--end .col-lg-8-->
                <div class="col-lg-4">
                    <div class="service_sidebar default_sidebar">
                        <div class="sidebar_widget category mb_30">
                            <h3>Categories</h3>
                            <ul>
                                <li><a href="#">Consumer Product <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Audit Marketing <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Banking Advising <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Risk Management <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Substantial Business <i class="fa fa-angle-right"></i></a></li>
                                <li><a href="#">Marketing Strategy <i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div><!--end .sidebar_widget.category-->
                        <div class="sidebar_widget need_help mb_30">
                            <h3>Need Help?</h3>
                            <p>Speak with a human to filling out a form? call corporate office and we will connect you with a team member who can help.</p>
                            <h2>888.333.0000</h2>
                        </div><!--end .sidebar_widget.need_help-->
                        <a href="#" class="btn btn-primary general_btn">Download Our Details</a>
                    </div><!--end .service_sidebar-->
                </div><!--end .col-lg-4-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .service_single_area-->
    <!-- ***************************
        Our Service Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
     <!-- ***************************
        Footer Area End
     *************************** -->


     @include ('layouts.user.script.chartScript')
</body>
</html>