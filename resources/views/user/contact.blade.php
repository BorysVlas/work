<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.mainStyle')


</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Contact</li>
                             </ol>
                         </nav>
                         <h1>Contact</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        Google Map Area Start
     *************************** -->
    <div class="contact_map" id="google_map">
        <div id="map"></div>
    </div><!--end .google_map-->
    <!-- ***************************
        Google Map Area End
     *************************** -->

    <!-- ***************************
        Contact Area Start
     *************************** -->
    <section class="contact_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="contact_info">
                        <div class="contact_box">
                            <div class="hero_section_title mb_60">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="19" height="13" viewBox="0 0 19 13">
                                    <g>
                                        <path d="M0.871,0.918 L7.442,9.994 L14.030,0.951 L0.871,0.918 Z" class="cls-1"></path>
                                        <path d="M5.871,3.918 L12.442,12.994 L19.030,3.950 L5.871,3.918 Z" class="cls-2"></path>
                                    </g>
                                </svg>
                                <h4>Contact Now</h4>
                                <h1>Feel free to make a
                                    <br>call request now</h1>
                            </div><!--end .hero_section_title-->
                            <form action="#" method="post">
                                <div class="row">
                                    <div class="col-lg-6 pr_10">
                                        <div class="input-group">
                                            <input type="text" placeholder="Business planing">
                                        </div><!--end .input-group-->
                                        <div class="input-group">
                                            <input type="text" placeholder="Your name">
                                        </div><!--end .input-group-->
                                        <div class="input-group">
                                            <input type="email" placeholder="Email address">
                                        </div><!--end .input-group-->
                                        <div class="input-group">
                                            <input type="text" placeholder="Phone number">
                                        </div><!--end .input-group-->
                                    </div><!--end .col-lg-6-->
                                    <div class="col-lg-6 pl_10">
                                        <div class="input-group">
                                            <textarea name="content" id="content" placeholder="Write message"></textarea>
                                        </div><!--end .input-group-->
                                        <div class="input-group">
                                            <button class="btn btn-primary general_btn" type="submit">Get Started</button>
                                        </div><!--end .input-group-->
                                    </div><!--end .col-lg-6-->
                                </div><!--end .row-->
                            </form>
                        </div><!--end .contact_box-->
                        <div class="contact_details_info">
                            <p>For any inquiries call now</p> <i class="icon-phone-call"></i> <strong>888.333.0000</strong>
                        </div><!--end .contact_details_info-->
                    </div><!--end .contact_info-->
                </div><!--end .col-lg-12-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .contact_area-->
    <!-- ***************************
        Contact Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
     <!-- ***************************
        Footer Area End
     *************************** -->


     @include ('layouts.user.script.mapScript')
    


</body>
</html>