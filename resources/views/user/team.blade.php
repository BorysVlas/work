<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.mainStyle')
</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Pages</li>
                                 <li class="breadcrumb-item active" aria-current="page">Our Team</li>
                             </ol>
                         </nav>
                         <h1>Our team</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        Team Member Area Start
     *************************** -->
    <section class="team_member_area sub_padding section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Christine Eve</h3>
                                <p>Officer</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Kevin Smith</h3>
                                <p>Consultant</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Jessica Brown</h3>
                                <p>Director</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Mark Hardson</h3>
                                <p>Manager</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->

                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>John Dave</h3>
                                <p>Officer</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Sarah Albert</h3>
                                <p>Officer</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Daniel Bond</h3>
                                <p>Director</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
                <div class="col-lg-3 col-md-6">
                    <div class="team_box">
                        <div class="img_box">
                            <img src="assets/images/teams/team-1.jpg" alt="Team Image">
                            <ul class="overlay">
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Linkedin</a>
                                </li>
                            </ul>
                        </div><!--end .img_box-->
                        <div class="team_details_box">
                            <div class="details_bg text-center">
                                <h3>Sabrina Rose</h3>
                                <p>Manager</p>
                            </div><!--end .details_bg-->
                        </div><!--end .team_details_box-->
                    </div><!--end .team_box-->
                </div><!--end .col-lg-3-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .team_member_area-->
    <!-- ***************************
        Team Member Area End
     *************************** -->

    <!-- ***************************
        Helping Over Area Start
     *************************** -->
    <section class="helping_over_area section_padding light_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <h1>Helping over <strong>786,000</strong> businesses in USA</h1>
                </div><!--end .col-lg-9-->
                <div class="col-lg-3 col-md-4 text-right">
                    <a href="#" class="btn btn-primary general_btn">Get Started</a>
                </div><!--end .col-lg-3-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .helping_over_area-->
    <!-- ***************************
        Helping Over Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
    
     <!-- ***************************
        Footer Area End
     *************************** -->



    @include ('layouts.user.script.mainScript')
</body>
</html>