<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.lightboxStyle')
</head>
<body>

    <!-- ***************************
        PreLoader Area Start
     *************************** -->
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!--end .spinner-->
    <!-- ***************************
        PreLoader Area Start
     *************************** -->

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->

    <!-- ***************************
        Breadcrumb Area Start
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Pages</li>
                                 <li class="breadcrumb-item active" aria-current="page">Our Gallery</li>
                             </ol>
                         </nav>
                         <h1>Our gallery</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <!-- ***************************
        Breadcrumb Area End
     *************************** -->

    <!-- ***************************
        Gallery Area Start
     *************************** -->
     <section class="gallery_area section_padding">
         <div class="container">
             <div class="row">
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 1">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 2">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 3">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 4">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 5">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 6">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 7">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 8">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
                 <div class="col-lg-4 col-md-6">
                     <div class="gallery_box">
                         <img src="assets/images/gallery/gl-1.jpg" alt="Gallery">
                         <div class="overlay">
                             <a href="assets/images/gallery/gl-1.jpg" class="lightbox" title="Gallery 9">
                                 <i class="fa fa-plus"></i>
                             </a>
                         </div><!--end .overlay-->
                     </div><!--end .gallery_box-->
                 </div><!--end .col-lg-4-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .gallery_area-->
    <!-- ***************************
        Gallery Area End
     *************************** -->

     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
     <!-- ***************************
        Footer Area End
     *************************** -->


     @include ('layouts.user.script.lightboxScript')
</body>
</html>