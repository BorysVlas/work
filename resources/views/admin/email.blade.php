@extends('layouts.admin.main')

@section('content')
    <div class="content-wrapper">
            <div class="panel-body" >
                <table class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                    <tr>
                        <th>Email adress</th>
                    </tr>
                    @foreach ($emails as $email)
                        <tr>
                            <td>{{ $email }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
    </div>
@endsection