<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vizeon - Business Consulting HTML Template</title>
    @include ('layouts.user.style.mainStyle')
</head>
<body>

    <!-- ***************************
        Header Area Start
     *************************** -->
     @include ('layouts.user.header')
    <!-- ***************************
        Header Area End
     *************************** -->
     <section class="breadcrumb_area section_padding text-center">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="breadcrumb_box">
                         <nav aria-label="breadcrumb">
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Pages</li>
                                 <li class="breadcrumb-item active" aria-current="page">Our Team</li>
                             </ol>
                         </nav>
                         <h1>Our team</h1>
                     </div><!--end .breadcrumb_box-->
                 </div><!--end .col-lg-12-->
             </div><!--end .row-->
         </div><!--end .container-->
     </section><!--end .breadcrumb_area-->
    <section class="team_member_area sub_padding section_padding">
        <div class="container">
            <div class="row">
                <?php if(isset($errors) and is_array($errors)): ?>
                    <ul>
                        <?php foreach ($errors as $value): ?>
                            <li><?php echo $value; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <form action="login" method="post" style="margin: 0 auto">
                    {{csrf_field()}}
                    <input type="text" class="form-control @error('login') is-invalid @enderror" name="login" placeholder="Login" required style="text-align:center;" autocomplete="login" autofocus>

                    @error('login') 
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required style="text-align:center;" autocomplete="current-password">
                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <hr>
                    <input type="submit" name="submit" value="Login" method="post">
                    <input type="reset" value="Reset" method="post">
                </form>
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .team_member_area-->
     <!-- ***************************
        Footer Area Start
     *************************** -->
     @include ('layouts.user.footer')
    
     <!-- ***************************
        Footer Area End
     *************************** -->



    @include ('layouts.user.script.mainScript')
</body>
</html>