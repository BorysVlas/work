<!doctype html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#3e454c">
	
	<title>Harmony - Free responsive Bootstrap admin template by Themestruck.com</title>

	<!-- Font awesome -->
	<link rel="stylesheet" href="adminStyle/css/font-awesome.min.css">
	<!-- Sandstone Bootstrap CSS -->
	<link rel="stylesheet" href="adminStyle/css/bootstrap.min.css">
	<!-- Bootstrap Datatables -->
	<link rel="stylesheet" href="adminStyle/css/dataTables.bootstrap.min.css">
	<!-- Bootstrap social button library -->
	<link rel="stylesheet" href="adminStyle/css/bootstrap-social.css">
	<!-- Bootstrap select -->
	<link rel="stylesheet" href="adminStyle/css/bootstrap-select.css">
	<!-- Bootstrap file input -->
	<link rel="stylesheet" href="adminStyle/css/fileinput.min.css">
	<!-- Awesome Bootstrap checkbox -->
	<link rel="stylesheet" href="adminStyle/css/awesome-bootstrap-checkbox.css">
	<!-- Admin Stye -->
	<link rel="stylesheet" href="adminStyle/css/style.css">

	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div class="brand clearfix">
		<a href=admin class="logo"><img src="adminStyle/img/logo.jpg" class="img-responsive" alt=""></a>
		<span class="menu-btn"><i class="fa fa-bars"></i></span>
		<ul class="ts-profile-nav">
			<li><a href="#">Help</a></li>
			<li><a href="#">Settings</a></li>
			<li class="ts-account">
				<a href="#"><img src="adminStyle/img/ts-avatar.jpg" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
				<ul>
					<li><a href="#">My Account</a></li>
					<li><a href="#">Edit Account</a></li>
					<li><a href="#">Logout</a></li>
				</ul>
			</li>
		</ul>
    </div>
    
    <div class="ts-main-content">
		<nav class="ts-sidebar">
			<ul class="ts-sidebar-menu">
				<li class="ts-label">Search</li>
				<li>
					<input type="text" class="ts-sidebar-search" placeholder="Search here...">
				</li>
				<li class="ts-label">Main</li>
				<li class="open"><a href="admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li><a href="#"><i class="fa fa-desktop"></i> UI Elements</a>
					<ul>
						<li><a href="panels">Panels and Wells</a></li>
						<li><a href="buttons">Buttons</a></li>
						<li><a href="notifications">Notifications</a></li>
						<li><a href="typography">Typography</a></li>
						<li><a href="icon">Icon</a></li>
						<li><a href="grid">Grid</a></li>
					</ul>
				</li>
				<li><a href="tables"><i class="fa fa-table"></i> Tables</a></li>
				<li><a href="forms"><i class="fa fa-edit"></i> Forms</a></li>
				<li><a href="charts"><i class="fa fa-pie-chart"></i> Charts</a></li>
				<li><a href="#"><i class="fa fa-sitemap"></i> Multi-Level Dropdown</a>
					<ul>
						<li><a href="#">2nd level</a></li>
						<li><a href="#">2nd level</a></li>
						<li><a href="#">3rd level</a>
							<ul>
								<li><a href="#">3rd level</a></li>
								<li><a href="#">3rd level</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="#"><i class="fa fa-files-o"></i> Sample Pages</a>
					<ul>
						<li><a href="blank">Blank page</a></li>
						<li><a href="login">Login page</a></li>
						<li>
							<a href="logout"
									onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
									{{ __('Logout') }} 
							</a>
							<form id="logout-form" action="logout" method="POST" style="display: none;">
                                @csrf
                            </form>
						</li>
					</ul>
				</li>

				<!-- Account from above -->
				<ul class="ts-profile-nav">
					<li><a href="#">Help</a></li>
					<li><a href="#">Settings</a></li>
					<li class="ts-account">
						<a href="#"><img src="adminStyle/img/ts-avatar.jpg" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
						<ul>
							<li><a href="#">My Account</a></li>
							<li><a href="#">Edit Account</a></li>
							<li>
								<a href="logout"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }} 
								</a>
							</li>
						</ul>
					</li>
				</ul>
				<li><a href="email"><i class="fa fa-pie-chart"></i>Users Emails</a></li>

			</ul>
        </nav>
        @yield('content');
    </div>

	

	<!-- Loading Scripts -->
	<script src="adminStyle/js/jquery.min.js"></script>
	<script src="adminStyle/js/bootstrap-select.min.js"></script>
	<script src="adminStyle/js/bootstrap.min.js"></script>
	<script src="adminStyle/js/jquery.dataTables.min.js"></script>
	<script src="adminStyle/js/dataTables.bootstrap.min.js"></script>
	<script src="adminStyle/js/Chart.min.js"></script>
	<script src="adminStyle/js/fileinput.js"></script>
	<script src="adminStyle/js/chartData.js"></script>
	<script src="adminStyle/js/main.js"></script>
	@yield('script')

</body>

</html>