    <header class="header_area">
        <div class="header_top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="header_top_flex flex">
                            <div class="header_flex_box left">
                                <p><i class="icon-phone-call phone"></i> 888 999 0000</p>
                            </div><!--end .header_flex_box.left-->
                            <div class="header_flex_box middle">
                                <a href="index">
                                    <img src="assets/images/logo.png" alt="Logo">
                                </a>
                            </div><!--end .header_flex_box.middle-->
                            <div class="header_flex_box right">
                                <div class="drop-down">
                                    <div class="select_btn_group" id="select_img_op">
                                        <a class="select_dropdown_toggle" href="#">
                                            <img src="assets/images/flag-2.png" alt="Flag Icon"> English <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="select_dropdown_menu">
                                            <li><a href="javascript:void(0);">
                                                <img src="assets/images/flag-2.png" alt="Flag Icon" /> English</a>
                                            </li>
                                            <li><a href="javascript:void(0);">
                                                <img src="assets/images/flag-2.png" alt="Flag Icon" /> France</a>
                                            </li>
                                            <li><a href="javascript:void(0);">
                                                <img src="assets/images/flag-2.png" alt="Flag Icon" /> China</a>
                                            </li>
                                            <li><a href="javascript:void(0);">
                                                <img src="assets/images/flag-2.png" alt="Flag Icon" /> Mexico</a>
                                            </li>
                                        </ul>
                                    </div><!--end .select_btn_group-->
                                </div><!--end .drop-down-->
                            </div><!--end .header_flex_box-->
                            <div class="header_flex_box right_two">
                                <div class="main_menu_area">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <nav class="navbar navbar-expand-lg navbar-light">
                                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                                        <span class="navbar-toggler-icon"></span>
                                                        <span class="navbar-toggler-icon"></span>
                                                        <span class="navbar-toggler-icon"></span>
                                                    </button>
                                                    <div class="collapse navbar-collapse">
                                                        <span class="close_menu">X</span>
                                                        <ul class="navbar-nav">
                                                            <li class="nav-item active">
                                                                <a class="nav-link dropdown-toggle" href="/" >
                                                                    Home
                                                                </a> <i class="fa fa-angle-down responsive_icon"></i>
                                                            </li>
                                                            <li class="nav-item ">
                                                                <a class="nav-link dropdown-toggle" href="service">
                                                                    Our Services
                                                                </a> <i class="fa fa-angle-down responsive_icon"></i>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a class="dropdown-item" href="service">Our Services</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="dropdown-item" href="service">Service Single</a>
                                                                    </li>
                                                                </ul><!--end .dropdown-menu-->
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link dropdown-toggle" href="about">
                                                                    Pages
                                                                </a> <i class="fa fa-angle-down responsive_icon"></i>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a class="dropdown-item" href="about">About Us</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="dropdown-item" href="about-2">About Us 2</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="dropdown-item" href="team">Our Team</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="dropdown-item" href="gallery">Gallery</a>
                                                                    </li>
                                                                </ul><!--end .dropdown-menu-->
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link dropdown-toggle" href="case">
                                                                    Our Cases
                                                                </a> <i class="fa fa-angle-down responsive_icon"></i>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a class="dropdown-item" href="case">Our Cases</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="dropdown-item" href="case-single">Case Single</a>
                                                                    </li>
                                                                </ul><!--end .dropdown-menu-->
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link dropdown-toggle" href="blog-grid">
                                                                    Blog
                                                                </a> <i class="fa fa-angle-down responsive_icon"></i>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a class="dropdown-item" href="blog-grid">Blog</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="dropdown-item" href="blog-sidebar">Blog With Sidebar</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="dropdown-item" href="blog-single">Blog Single</a>
                                                                    </li>
                                                                </ul><!--end .dropdown-menu-->
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link dropdown-toggle" href="contact">
                                                                    Contact
                                                                </a> <i class="fa fa-angle-down responsive_icon"></i>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a class="dropdown-item" href="contact">Contact V1</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="dropdown-item" href="contact-2">Contact V2</a>
                                                                    </li>
                                                                </ul><!--end .dropdown-menu-->
                                                            </li>
                                                        </ul>
                                                    </div><!--end .collapse-->
                                                </nav>
                                            </div><!--end .col-lg-12-->
                                        </div><!--end .row-->
                                    </div><!--end .container-->
                                </div><!--end .main_menu_area-->
                            </div><!--end .header_flex_box.middle-->
                        </div><!--end .header_top_flex-->
                    </div><!--end .col-lg-12-->
                </div><!--end .row-->
            </div><!--end .container-->
        </div><!--end .header_top-->
        <div class="main_menu_area scroll_fixed">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                                <span class="navbar-toggler-icon"></span>
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse">
                                <span class="close_menu">X</span>
                                <ul class="navbar-nav">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="/" >
                                            Home
                                        </a> <i class="fa fa-angle-down responsive_icon"></i>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link" href="service">
                                            Our Services
                                        </a> <i class="fa fa-angle-down responsive_icon"></i>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="case">
                                            Our Cases
                                        </a> <i class="fa fa-angle-down responsive_icon"></i>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="dropdown-item" href="case">Our Cases</a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" href="case-single">Case Single</a>
                                            </li>
                                        </ul><!--end .dropdown-menu-->
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="blog-grid">
                                            Blog
                                        </a> <i class="fa fa-angle-down responsive_icon"></i>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="contact">
                                            Contact
                                        </a> <i class="fa fa-angle-down responsive_icon"></i>
                                    </li>
                                </ul>
                            </div><!--end .collapse-->
                        </nav>
                    </div><!--end .col-lg-12-->
                </div><!--end .row-->
            </div><!--end .container-->
        </div><!--end .main_menu_area-->
    </header><!--end .header_area-->
    @include('layouts.user.modal')